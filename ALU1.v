module ALU1(a,b,control,result,zero);
input [31:0] a,b;
input [2:0] control;
output [31:0] result;
output zero;
reg [31:0] result; 
reg zero;

always @ (a or b or control)
begin
case (control)
3'b000 : result = a & b;
3'b001 : result = a | b;
3'b010 : result = a + b;
3'b110 : result = a - b;
3'b111 : if(a<b)
         result = 32'd1;
         else
         result = 32'd0;
default : result = 32'dx;
endcase
if (result == 32'd0)
zero = 2'd1;
else
zero = 2'd0;
end

endmodule


  
