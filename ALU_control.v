module ALU_control(ALUOp,fn_code,control);
   input [1:0] ALUOp;
   input [5:0] fn_code;
   output [2:0] control;
   reg [2:0] 	control;
 
   parameter Add = 6'd32, Sub = 6'd34, AND = 6'd36, OR = 6'd37, slt = 6'd42;  

   always @ (ALUOp or fn_code)
     begin
	case (ALUOp)
	  2'b00 : control = 3'b010;
	  2'b01 : control = 3'b110;
	  2'b10 : case (fn_code)
		    Add : control = 3'b010;
		    Sub : control = 3'b110;
		    AND : control = 3'b000;
		    OR  : control = 3'b001;
		    slt : control = 3'b111; 
		    default : control = 3'bxxx;           
		  endcase
	  default : control = 3'bxxx;
	endcase
     end
endmodule