module Control(opcode,RegDst,Jump,Branch,MemRead,MemtoReg,ALUOp,MemWrite,ALUSrc,RegWrite);
   input [5:0] opcode;
   output reg  RegDst,Jump,Branch,MemRead,MemtoReg,MemWrite,ALUSrc,RegWrite;
   output reg [1:0] ALUOp; 
   
   parameter R_type = 6'd0, lw=6'd35, sw=6'd43, beq = 6'd4, j=6'd2, addi=6'd8;
   always @ (opcode)
     begin
	case (opcode)
	  R_type: 
	    begin
               RegDst = 1'b1;
               Jump = 1'b0;
               Branch = 1'b0;
               MemRead = 1'b0;
               MemWrite= 1'b0;
               
               MemtoReg= 1'b0;
               ALUSrc = 1'b0;
               RegWrite = 1'b1;
               ALUOp=2'b10;
            end // case: R_type
	  
	  lw: 
	    begin
               RegDst = 1'b0;
               Jump = 1'b0;
               Branch = 1'b0;
               MemRead = 1'b1;
               MemWrite= 1'b0;
	       
               MemtoReg= 1'b1;
               ALUSrc = 1'b1;
               RegWrite = 1'b1;
               ALUOp = 2'b00;
            end // case: lw
	  
	  sw:
	    begin
               RegDst = 1'bx;
               Jump = 1'b0;
               Branch = 1'b0;
               MemRead = 1'b0;
               MemWrite= 1'b1;
	       
               MemtoReg= 1'bx;
               ALUSrc = 1'b1;
               RegWrite = 1'b0;
               ALUOp = 2'b00;
            end // case: sw
	  
	  beq:
	    begin
               RegDst = 1'bx;
               Jump = 1'b0;
               Branch = 1'b1;
               MemRead = 1'b0;
               MemWrite= 1'b0;
	       
               MemtoReg= 1'bx;
               ALUSrc = 1'b0;
               RegWrite = 1'b0;
               ALUOp = 2'b01;
            end // case: beq
	  
	  j:
	    begin
               RegDst = 1'bx;
               Jump = 1'b1;
               Branch = 1'b0;
               MemRead = 1'b0;
               MemWrite = 1'b0;
	       
               MemtoReg = 1'bx;
               ALUSrc = 1'bx;
               RegWrite = 1'b0;
               ALUOp = 2'b00;
            end // case: j
	  
	  addi:
	    begin
	       RegDst = 1'b0;
	       Jump = 1'b0;
	       Branch = 1'b0;
	       MemRead = 1'bx;
	       MemtoReg = 1'b0;

	       ALUOp = 2'b00;
	       MemWrite = 1'b0;
	       ALUSrc = 1'b1;
	       RegWrite = 1'b1;
	    end // case: addi
	  
	  default: begin
	     RegDst = 1'bx;
             Jump = 1'bx;
             Branch = 1'bx;
             MemRead = 1'bx;
             MemWrite= 1'bx;
	     
             MemtoReg= 1'bx;
             ALUSrc = 1'bx;
             RegWrite = 1'bx;
             ALUOp = 2'bxx;
          end
	endcase
     end
endmodule