//Non-pipelined code, clk reset not used
module DataMem(out, add_in, data_in, mem_read, mem_write);
   //Definition of output
   output reg [31:0] out;

   //Definition of inputs
   input [31:0]      add_in;
   input [31:0]      data_in;
   input 	     mem_read;
   input 	     mem_write;
   
   //Definition of memory storage
   reg [31:0] 	     ram [31:0];

   initial
     begin
	ram[0][31:0] = 6'd2;
	ram[1][31:0] = 6'd14;
	ram[2][31:0] = 6'd18;
	ram[3][31:0] = 6'd20;
	ram[4][31:0] = 6'd15;
	ram[5][31:0] = 6'd12;
	ram[6][31:0] = 6'd32;
	ram[7][31:0] = 6'd30;
	ram[8][31:0] = 6'd32;
	ram[9][31:0] = 6'd12;
	ram[10][31:0] = 6'd13;
	ram[11][31:0] = 6'd44;
	ram[12][31:0] = 6'd40;
	ram[13][31:0] = 6'd41;
	ram[14][31:0] = 6'd39;
	ram[15][31:0] = 6'd17;
	ram[16][31:0] = 6'd25;
	ram[17][31:0] = 6'd5;
	ram[18][31:0] = 6'd1;
	ram[19][31:0] = 6'd22;
	ram[20][31:0] = 6'd29;
	ram[21][31:0] = 6'd27;
	ram[22][31:0] = 6'd34;
	ram[23][31:0] = 6'd43;
	ram[24][31:0] = 6'd39;
	ram[25][31:0] = 6'd9;
	ram[26][31:0] = 6'd19;
	ram[27][31:0] = 6'd6;
	ram[28][31:0] = 6'd8;
	ram[29][31:0] = 6'd26;
	ram[30][31:0] = 6'd12;
	ram[31][31:0] = 6'd43;
     end // initial begin

   always @(*)
     begin
	if (mem_read == 1 && mem_write == 0)
	  out = ram[add_in];
	else if (mem_read == 0 && mem_write == 1)
	  ram[add_in] = data_in;
     end
endmodule // DataMem
