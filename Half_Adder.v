module Half_Adder(PC, in, Add);
   input [31:0] PC;
   input [31:0] in;

   output [31:0] Add;

   assign Add = PC + in;
endmodule 
