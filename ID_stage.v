module ID_stage(clk, rst, instruction, jump_offset, Rs, Rt, Rd,
                RegDst, Jump, Branch, MemRead, MemtoReg, ALUOp,
                MemWrite, ALUSrc, RegWrite,
                fn_code, out_signextend, shift_out);
   input clk, rst;
   input [31:0] instruction;

   output [25:0] jump_offset;
   
   output [4:0]      Rs, Rt, Rd;
   output            RegDst, Jump, Branch, MemRead, MemtoReg, MemWrite, ALUSrc, RegWrite;
   output [31:0]     out_signextend;
   output [31:0]     shift_out;
   output [1:0]      ALUOp;
   output [5:0]      fn_code;
   
   wire [5:0]        opcode;
   wire [15:0]       offset;       
   
   reg [31:0]        instruction_reg;

   always @(posedge clk)
     begin
        if(rst)
          instruction_reg <= 0;
        else
          begin
             instruction_reg <= instruction;
          end
     end
     
   // bit select different modules
   assign opcode = instruction_reg [31:26];
   assign Rs     = instruction_reg [25:21];
   assign Rt     = instruction_reg [20:16];
   assign Rd     = instruction_reg [15:11];
   //assign shmt   = instruction_reg [10:6];
   assign fn_code = instruction_reg [5:0];
   assign offset = instruction_reg [15:0];
   assign jump_offset = instruction_reg [25:0]; 
   
   // sign extension   
   assign out_signextend [15:0] = offset[15:0];
   assign out_signextend [31:16] = offset[15];   
   // Shift Register
   assign shift_out = out_signextend << 2;
   //Already done inside IF_stage.v
   //assign  jump_address = jump_offset << 2;
   Control cont1(opcode,RegDst,Jump,Branch,MemRead,MemtoReg,ALUOp,MemWrite,ALUSrc,RegWrite);
endmodule
  
