module IF_stage(clk, rst, jump, jump_offset, Branch, zero, shift_out, 
                pc, instruction);
   input clk;
   input rst;
   input jump;
   input [25:0] jump_offset;
   input        Branch;
   input        zero;
   input [31:0] shift_out;
   
   output reg [31:0] pc;
   output [31:0] instruction;
   
   always @ (posedge clk or posedge rst)
     begin
        if (rst) 
          begin
	     pc <= 32'b0;
          end 
        else 
          begin
             pc <= pc + 4;
             if(jump)
               pc <= {pc[31-:4], (jump_offset << 2)};
             if(Branch & zero)
               pc <= pc + shift_out;
          end
     end // always @ (posedge clk or posedge rst)
   
   //instruction memory / ROM to fetch the next instruction
   InstMem imem(.pc(pc), .instruction(instruction));
   
   /*
    initial
    $monitor("IF_stage clk %b rst %b pc %b instr %b jump %b jump_offset %b Branch %b sel1 %b", clk, rst, pc, instruction, jump, jump_offset, Branch, (Branch & zero));
    */
endmodule // IF_stage

