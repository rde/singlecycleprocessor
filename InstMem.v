//out : 32 bit instruction
//in : program counter 8 bit, that's the reason why we need to do PC <= PC + 4

//clk : not used, asynchronous logic

//First version : does not use ROM and users direct addressing

//Second version : Load up assembly code into ROM

module InstMem(pc, instruction);
   
   output reg [31:0] instruction;
   
   //Instruction memory location
   input [31:0]       pc;

   wire [31:0]        addr = pc;
   
   always @(*)
     case (addr)
       'b000100: instruction = 32'b10001110010010000000000000000100; //lw $t0, 4($s2)
       'b001000: instruction = 32'b10001110011010010000000000000100; //lw $t1, 5($s3)
       'b001100: instruction = 32'b00000001000010011000000000100000; //add $s0, $t0, $t1
       'b010000: instruction = 32'b00001000000000000000000000001010; //j 1010
       'b010100: instruction = 32'b00000010000010000100000000100000; //add $t0, $t0, $s0
       'b101000: instruction = 32'b00000001000010011000100000101000; //sub $s1, $t0, $t1
       'b101100: instruction = 32'b00100001001100000000000000000101; //addi $s0, $t1, 5
       'b110100: instruction = 32'b00010000110001100000000000000010; //beq R6, R6, 2
       'b111000: instruction = 32'b00000001000010010101000000100100; //add $t0, $t0, $s0
       'b1000000: instruction = 32'b10001110010010000000000000000100; //lw $t0, 4($s2)
       
      /* 'b000100: instruction = 32'b00001000000000000000000000001100; //j 1100
       'b001000: instruction = 32'b00000010000010000100000000100000; //add $t0, $t0, $s0
       'b001100: instruction = 32'b10001110010010000000000000000100; //lw $t0, 4($s2)
       'b010000: instruction = 32'b10101110010010000000000000001000; //sw $t0, 8($s3)
       'b110000: instruction = 32'b10001110010010000000000000000100; //lw $t0, 4($s2)
       'b110100: instruction = 32'b00010000110001100000000000000010; //beq R6, R6, 2
       'b111000: instruction = 32'b00000010000010000100000000100000; //add $t0, $t0, $s0
       'b111100: instruction = 32'b10101110010010000000000000001000; //sw $t0, 8($s3)
       'b1000000: instruction = 32'b10001110010010000000000000000100; //lw $t0, 4($s2)
       'b1000100: instruction = 32'b10101110010010000000000000001000; //sw $t0, 8($s3)
       'b1001000: instruction = 32'b10001110010010000000000000000100; //lw $t0, 4($s2)
       'b1001100: instruction = 32'b00100001001100000000000000000101; //addi $s0, $t1, 5
       'b1010000: instruction = 32'b10101110010010000000000000001000; //sw $t0, 8($s3) */
       default: instruction = 'b0;
     endcase // case (addr)

   initial
   $monitor("InstMem pc %b instr %b", pc, instruction);
endmodule // InstMem
