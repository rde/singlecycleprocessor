module MIPS (clk, rst, PC);
   input             clk;
   input             rst;

   output [31:0]     PC;
   
   //wire [31:0] mips_dataout;
   //wire [31:0] mips_addrsout;
   wire [31:0] 	 instruction;
   
   //register wires
   wire [31:0]   Read_data1;
   wire [31:0] 	 Read_data2;
   wire [4:0] 	 Write_register;

   // breaking for R_type
   wire [5:0] 	 opcode;
   wire [5:0] 	 fn_code;
   wire [4:0] 	 Rs;
   wire [4:0] 	 Rt;
   wire [4:0] 	 Rd;
   wire [4:0] 	 shmt; 

   // instruction I_type
   wire [15:0] 	 offset;
   wire [25:0] 	 jump_offset;
   wire [31:0] 	 out_signextend;
   wire [31:0] 	 shift_out;
   wire [31:0] 	 jump_address;

   //control variables
   wire 	 RegDst;
   wire 	 Jump;
   wire 	 Branch;
   wire 	 MemRead;
   wire 	 MemtoReg;
   wire [1:0] 	 ALUOp;
   wire 	 MemWrite;
   wire 	 ALUSrc;
   wire 	 RegWrite;
   
   //more control signals
   wire [2:0] 	 control;
   wire 	 zero;
   
   //mux input and output variables
   
   wire [31:0] 	 mux1_out;
   //wire [31:0] mux2_out; //since PC_in is defined
   wire [31:0] 	 mux3_out;
   wire [31:0]    mux4_out; //same as Write_data

   //PC variables
   wire [31:0] 	 Add;
   wire [31:0] 	 b_add;
   
   //Data output
   wire [31:0] 	 data_out;
   wire [31:0] 	 result;
   
   //Final output of each instruction
   
   // datapath signals
   // module instantiations
   //address input
   
   
   //Final integration
   
   //Instruction fetch stage : synchronized on clock
   IF_stage IF_stage_inst(.clk(clk),
                          .rst(rst),
                          .jump_offset(jump_offset),
                          .jump(Jump),
                          .Branch(Branch),
                          .zero(zero),
                          .shift_out(shift_out),
                          .pc(PC),
                          .instruction(instruction));
   //initial
   //$monitor($time, " clk %b rst %b jump_offset %b jump %b PC %b instr %b", clk, rst, jump_offset, Jump, PC, instruction);

   //Instruction decode stage : synchronized on clock
   ID_stage ID_stage_inst(.clk(clk),
                          .rst(rst),
                          .instruction(instruction),
                          .jump_offset(jump_offset),
                          .Rs(Rs),
                          .Rt(Rt),
                          .Rd(Rd),
                          .RegDst(RegDst),
                          .Jump(Jump),
                          .Branch(Branch),
                          .MemRead(MemRead),
                          .MemtoReg(MemtoReg),
                          .ALUOp(ALUOp),
                          .MemWrite(MemWrite),
                          .ALUSrc(ALUSrc),
                          .RegWrite(RegWrite),
                          .fn_code(fn_code),
                          .out_signextend(out_signextend),
                          .shift_out(shift_out));
   //initial
   //$monitor($time, " rst %b clk %b instr %b Rs %b Rt %b Rd %b RegWrite %b RegDst %b out_signextend %b shift_out %b", rst, clk, instruction, Rs, Rt, Rd, RegWrite, RegDst, out_signextend, shift_out);

   //Register file : register write synchronized using clock
   Registers reg1(clk, rst, RegWrite, RegDst, Rs, Rt, Rd, mux4_out, Read_data1, Read_data2);
   
   mux  mux3(ALUSrc,Read_data2,out_signextend,mux3_out); 
   ALU_control aluCont(ALUOp,fn_code,control);
   ALU1 alu1inst(Read_data1, mux3_out, control, result, zero);
   
   //Writeback stage : uses RAM. no clock synchronizaiton for non-pipelined code
   DataMem dm(.out(data_out), .add_in(result), .data_in(Read_data2), .mem_read(MemRead), .mem_write(MemWrite));   
   mux mux4(.sel(MemtoReg), .a(result), .b(data_out), .y(mux4_out));
   
   initial
     $monitor($time, " rst %b clk %b instr %b RegWrite %d RegDst %b Rs %d Rt %d Rd %d jump_offset %b jump %b mux4_out %d Read_data1 %d Read_data2 %d data_out %d result %d MemRead %b MemWrite %b", rst, clk, instruction, RegWrite, RegDst, Rs, Rt, Rd, jump_offset, Jump, mux4_out, Read_data1, Read_data2, data_out, result, MemRead, MemWrite);
endmodule
