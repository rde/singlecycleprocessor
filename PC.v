 
// Simple 32-bit register
module PC (clk, PC_in,PC_out);
   input               clk;
   input [31:0] 	PC_in;
   output reg [31:0] 	PC_out;
    //reg     [31:0]	PC_out;
   
   always @(posedge clk)
     begin
        //if (reset) PC_out <= 0;
        PC_out <= PC_in;
     end

endmodule