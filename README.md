Project: Synchronous single cycle CPU implementing MIPS 32-bit architecture

-------------------------------------------------------
Copyright © 2012 by Ruchira De and Namita Godara

PLEASE SEE LICENCE FILE FOR LICENCE RESTRICTIONS

Description:
This project implements a verilog module for a single cycle CPU implementing 
MIPS 32-bit architecture which is synchronized with a clock.

User provides 32-bit MIPS instructions and the results can be viewed in the GUI.

Code limitation: The code implements add, sub, addi, lw, sw, and, or, nor, beq,
bne, slt, j and jal MIPS instructions.

To compile:
vcs *.v +v2k

To simulate:
./simv

To debug before going to VCS waveform viewer:
vcs *.v -debug_all +v2k

To go to VCS waveform viewer:
./simv -gui &

Simulated waveforms are in ./waveforms folder.

Questions/Comments:
For any question/comment related to the project send an email to the author:
Ruchira De ruchira.de@gmail.com Namita Godara namigodara@gmail.com