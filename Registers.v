module Registers(clk, rst, RegWrite, RegDst,
                 Rs, Rt, Rd, mux4_out,
                 Read_data1, Read_data2);
   input clk;
   input rst;
   input RegWrite;
   input RegDst;
   input [31:0] mux4_out;
   
   input [4:0]  Rs, Rt, Rd;
   
   output [31:0] Read_data1, Read_data2;
   
   wire [4:0]    Write_register;
   reg [31:0]    reg_file [31:0]; 
   integer       i;

   mux_5bit m5_1(RegDst,Rt,Rd,Write_register);

   always @(posedge rst) //make all ZEROES
     begin
        for (i = 0; i < 32; i = i+1) 
          reg_file[i][31:0] <= 32'b0;
     end
   
   always @(posedge clk) 
     if (RegWrite && (Write_register != 0))
       begin
	  reg_file[Write_register] <= mux4_out;
       end
   
   assign Read_data1 = Rs ? reg_file[Rs] : 32'd0;
   assign Read_data2 = Rt ? reg_file[Rt] : 32'd0;

   //initial
   //$monitor($time, " rst %b clk %b Rs %b Rt %b Rd %b Write_register %b RegDst %b Read_data1 %b Read_data2 %b", rst, clk, Rs, Rt, Rd, Write_register, RegDst, Read_data1, Read_data2);
endmodule
