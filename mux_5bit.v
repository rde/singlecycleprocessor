 
module mux_5bit(RegDst, Rt, Rd, Write_register);
   
    input RegDst;
    input [4:0] Rt,Rd ;
    output [4:0] Write_register;

    assign Write_register = RegDst ? Rd : Rt;
endmodule 
