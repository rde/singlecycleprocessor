module tb();
   reg clk;
   reg reset;
   
   MIPS mips(clk, reset,);
   
   //clk signal
   initial
     begin
	clk = 1'b1;
	forever #1 clk = ~clk;
     end

   //reset signal
   initial
     begin
        reset = 1'b1; repeat (1) @ (posedge clk);
        reset = 1'b0; repeat (10) @ (posedge clk);
     end

   initial
     #25 $finish;
endmodule // tb
